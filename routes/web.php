<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('index.trangchu');
})->name('trangchu');
    

Route::group(['prefix' => 'user', 'middleware' => 'userlogin'], function(){

Route::get('/', function () {
    return view('user.home');
})->name('user');

Route::get('/contact', function () {
    return view('user.contact');
})->name('contact');

Route::get('/policy', function () {
    return view('user.policy');
})->name('policy');

});



Auth::routes();

Route::get('/home', 'HomeController@index');
